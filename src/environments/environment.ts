export const environment = {
  production: false,
  API_URL: 'http://localhost:8080/api',
  FRONT_URL: 'http://localhost:4200/',
  IMAGE_API: 'http://localhost:8080/images/',
  FILE_LOCATION_API: 'http://localhost:8080/document-location/dossier-numero-',
};
