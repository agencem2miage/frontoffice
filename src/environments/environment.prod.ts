export const environment = {
  production: true,
  API_URL: 'http://agencetourix.ddns.net/api',
  FRONT_URL: 'http://agencetourix.ddns.net/',
  IMAGE_API: 'http://agencetourix.ddns.net/images/',
  FILE_LOCATION_API:
    'http://agencetourix.ddns.net/document-location/dossier-numero-',
};
