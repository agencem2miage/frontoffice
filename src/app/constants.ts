import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';

/**
 * Liste des constantes de l'application
 */
@Injectable({
  providedIn: 'root',
})
export class Constants {
  /**
   * Points d'entrée de l'API
   */
  public API_ENDPOINTS = Object.freeze({
    // Utilisation GET, POST, PUT, DELETE
    USER: '/users',
    BAIL: '/bail',
    CONNEXION: '/connexion',
    BIEN: '/bien',
    IMAGES: '/images',
    FILE: '/file',
  });

  /**
   * Lien vers les différents localisations de fichier
   */
  public PATH_FILE_LOCATION = Object.freeze({
    BAIL: '/1.bail/',
    ETAT_LIEU: '/2.etat-lieu/',
    ASSURANCE_PROPRIETAIRE: '/3.assurance-proprietaire/',
    ASSURANCE_LOCATAIRE: '/4.assurance-proprietaire/',
    CONTRAT_GESTION: '/5.contrat-gestion/',
    DIAG_ENERGIE: '/6.diagnostique-energetique/',
    AUTRE: '/7.autre/',
  });

  //DATE
  /**
   * Date actuelle
   */
  public CURRENT_DATE = Date.now();

  //INFO du site
  /**
   * Url du Front
   */
  public FRONT_URL = environment.FRONT_URL;

  /**
   * Url actuelle de l'utilisateur
   */
  public CURRENT_URL = window.location.href;

  /**
   * Url de l'image
   */
  public IMAGE_URL = this.FRONT_URL + 'assets/images/';

  /**
   * Nom du site
   */
  public SITE_NAME = 'Agencetourix';

  /**
   * Année de fondation
   */
  public ORG_FUNDING_DATE = '2021';

  /**
   * Nom du fondateur
   */
  public FOUNDER_NAME = 'Alain Guimard';

  //CONTACT
  /**
   * Nom de contact
   */
  public CONTACT_NAME = 'John Doe';

  /**
   * email de contact
   */
  public CONTACT_EMAIL = 'john.doe@gmail.com';

  /**
   * Téléphone du contact
   */
  public CONTACT_PHONE = '06 05 07 08 09';

  /**
   * Adresse du contact
   */
  public CONTACT_ADDRESS = '10 rue de la paix';

  /**
   * Ville de contact
   */
  public CONTACT_CITY = 'Bordeaux';

  /**
   * Code postal du contact
   */
  public CONTACT_CP = '33000';

  //SEO
  /**
   * Nom du site
   */
  public SEO_SITE_TITLE = this.SITE_NAME + ' : ';

  /**
   * Description SEO
   */
  public SEO_SITE_DESCRIPTION = 'Description SEO';
}

/**
 * Enum des rôles
 */
export enum Roles {
  ADMIN = 1,
  AGENT = 2,
  PROPRIETAIRE = 3,
  LOCATAIRE = 4,
}

/**
 * Enum des types de biens
 */
export enum TypesBien {
  MAISON = 1,
  APPARTEMENT = 2,
  AUTRE = 3,
}

/**
 * enum des types de contrats
 */
export enum TypesContrat {
  LOCATION = 1,
}

/**
 * enum des types de pièces
 */
export enum TypesPiece {
  CUISINE = 1,
  SALLE_DE_BAIN = 2,
  SALON = 3,
  SALLE_A_MANGER = 4,
  SALLE_EAU = 5,
  CHAMBRE = 6,
  TOILETTES = 7,
}

/**
 * enum des régimes juridiques
 */
export enum RegimesJuridiques {
  MONOPROPRIETE = 1,
  COPROPRIETE = 2,
}

/**
 * enum des usages Locaux
 */
export enum UsagesLocaux {
  HABITATION = 1,
  MIXTE = 2,
}

/**
 * enum des la durée des bails
 */
export enum DureesBail {
  REDUITE = 1,
  TROIS_ANS = 2,
  SIX_ANS = 3,
}

/**
 * enum des civilités
 */
export enum Civilites {
  MADAME = 'Madame',
  MONSIEUR = 'Monsieur',
}

/**
 * enum des modalités de réglement des charges récupérables
 */
export enum ModalitesReglementChargesRecuperables {
  PROVISIONS_SUR_CHARGES = 'Provisions sur charges',
  PAIEMENT_PERIODIQUE = 'Paiement périodique',
  FORFAIT_CHARGE = 'Forfait charge',
}

/**
 * enum des échéances de paiement
 */
export enum EcheancesPaiement {
  ECHOIR = 'A échoir',
  ECHU = 'A terme échu',
}
