import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { ModalService } from './../services/modal.service';
import { TokenService } from './../services/token.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Composant Header
 */
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})

/**
 * Classe pour la navigation
 */
export class NavComponent implements OnInit {
  
  /**
   * 
   * @param tokenService Service pour le token
   * @param router Router
   * @param modalService Service pour le modal
   */
  constructor(
    private tokenService: TokenService,
    private router: Router,
    private modalService: ModalService
  ) { }

  /**
   * Boolean pour savoir si l'utilisateur est connecté ou non
   */
  isConnected = false;

  /**
   * Initialisation
   */
  ngOnInit(): void {
    this.isConnected = this.tokenService.getToken() !== '';
  }

  /**
   * Vérifie l'état de connexion de l'administrateur
   */
  ngDoCheck(): void {
    this.isConnected = this.tokenService.getToken() !== '';
  }

  /**
   * Fonction de déconnexion de l'utilisateur
   */
  logOut(): void {
    const confirmation = new Modal({
      type: ModalType.CONFIRMATION,
      title: 'Déconnexion',
      text: 'Souhaitez-vous vraiment vous déconnecter ?',
    });

    confirmation.confirm = () => {
      this.tokenService.logOut();
      this.router.navigate(['/']);
      this.isConnected = false;
    };

    this.modalService.confirmationModal(confirmation);
  }
}
