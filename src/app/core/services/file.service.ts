import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
/**
 *  Classe FileService
 */
export class FileService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  fileEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.FILE;
  //imagesEndpoint = environment.IMAGE_API;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Récupère la liste des fichiers de locations
   * @param bailId identifiant du bail
   * @returns une liste de fichier
   */
  getListeFileLocation(bailId: number[]): Observable<any> {
    return this.http
      .post(this.fileEndpoint + '/liste-file-location', { bailId }, httpOptions)
      .pipe(
        map((jsonResponse: any) =>
          jsonResponse.map((jsondeuxiemeResponse: any) => jsondeuxiemeResponse)
        )
      );
  }

  /**
   * Met à jour la liste des fichiers de locations
   * @param listFile Liste de fichier
   * @param bailId identifiant du bail
   * @returns Retourne un code 200 si l'update c'est réalisé, sinon une erreur
   */
  updateFileLocation(listFile: any, bailId: string): Observable<any> {
    return this.http.put(
      this.fileEndpoint + '/file-location',
      { listFile, bailId },
      httpOptions
    );
  }
}
