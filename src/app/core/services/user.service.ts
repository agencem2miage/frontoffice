import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Constants } from '../../constants';
import { environment } from '../../../environments/environment';
import { Utilisateur } from '../../shared/models/utilisateur.model';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
/**
 * Service de gestion des utilisateurs
 */
@Injectable({
  providedIn: 'root',
})
export class UserService {
  /**
   * Point d'entrée de l'API Agence pour la gestion des utilisateurs
   */
  userEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.USER;

  /**
   * Import des services nécessaires
   * @param http Service natif d'angular pour la création de requêtes HTTP
   * @param constants Fichier qui contient les constantes du projet
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Récupère un utilisateur auprès de l'API Agence
   * @param id Identifiant de l'utilisateur à récupérer
   * @returns L'utilisateur récupéré
   */
  updateUser(user: Utilisateur): Observable<any> {
    return this.http
      .put(this.userEndpoint + '/' + 'editProfile', { user }, httpOptions)
      .pipe(
        map(
          (jsonResponse: any) =>
            jsonResponse.map((jsonUser: any) => jsonUser)[0]
        )
      );
  }
}
