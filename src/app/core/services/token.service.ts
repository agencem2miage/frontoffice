import { Injectable } from '@angular/core';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  /**
   * Enregistre le token de connexion en localStorage
   * @param token Token de connexion récupéré depuis l'API
   */
  public saveToken(token: string): void {
    localStorage.removeItem('x-auth-token');
    localStorage.setItem('x-auth-token', token);
  }

  /**
   * Récupère le token de connexion depuis le localStorage
   * @returns Le token stocké
   */
  public getToken(): string {
    return localStorage.getItem('x-auth-token') || '';
  }

  /**
   * Enregistre l'utilisateur connecté en localStorage
   * @param user L'utilisateur connecté
   */
  public saveUser(user: any): void {
    localStorage.removeItem('x-auth-user');
    localStorage.setItem('x-auth-user', JSON.stringify(user));
  }

  /**
   * Récupère l'utilisateur connecté depuis le localStorage
   * @returns L'utilisateur connecté
   */
  public getUser(): any {
    let jsonUser:any = null;
    if (localStorage.getItem('x-auth-user') !== null) {
      jsonUser = JSON.parse(localStorage.getItem('x-auth-user') || '');
    }

    return jsonUser;
  }

  /**
   * Déconnecte un utilisateur en vidant le localStorage
   */
  public logOut(): void {
    localStorage.clear();
  }
}
