import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/constants';
import { map } from 'rxjs/operators';
import { Bien } from 'src/app/shared/models/bien.model';
import { environment } from 'src/environments/environment';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})

/**
 * Classe ImageService
 */
export class ImagesService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  imagesEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.IMAGES;
  //imagesEndpoint = environment.IMAGE_API;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Récupères les images associés à un bien
   * @param idBien identifiant d'un bien
   * @returns les images
   */
  getImages(idBien: number): Observable<any> {
    return this.http
      .get(this.imagesEndpoint + '/' + idBien)
      .pipe(map((jsonResponse: any) => jsonResponse));
  }
}
