import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

/**
 * Injectable
 */
@Injectable({
  providedIn: 'root',
})

/**
 * Service d'authentification
 */
export class AuthentificationService {
  /**
   * Point d'entrée de l'API pour la gestion de la connexion
   */
  connexionEndpoint =
    environment.API_URL + this.constants.API_ENDPOINTS.CONNEXION;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Fonction qui permet à un utilisateur de se connecter
   * @param login Login de l'utilisateur
   * @param password Mot de passe de l'utilsateur
   * @returns Si les authentification de connexion sont bonnes ou mauvaises
   */
  login(
    login: string,
    password: string
  ): Observable<{ user: any; token: string }> {
    const passwordCrypt = Md5.hashStr(password).toString();
    return this.http
      .post(
        this.connexionEndpoint + '/user',
        { login, passwordCrypt },
        httpOptions
      )
      .pipe(
        map((jsonResponse: any) => {
          /**
           * TODO : récupération du token + type d'utilisateur connecté
           */
          return {
            user: jsonResponse.user,
            token: jsonResponse.authenticationToken,
          };
        })
      );
  }
}
