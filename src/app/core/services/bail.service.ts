import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/constants';
import { map } from 'rxjs/operators';
import { Bien } from 'src/app/shared/models/bien.model';
import { environment } from 'src/environments/environment';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
/**
 * Classe BailService
 */
export class BailService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  bailEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.BAIL;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Récupère l'id d'un bail d'un propriétaire
   * @param idProprietaire identifiant d'un propriétaire
   * @returns un identifiant d'un bail d'un propriétaire
   */
  getBailIdProprietaire(idProprietaire: number): Observable<any> {
    return this.http
      .get(this.bailEndpoint + '/bailProprietaireId/' + idProprietaire)
      .pipe(
        map((jsonResponse: any) => jsonResponse.map((jsonId: any) => jsonId.id))
      );
  }

  /**
   * Récupère l'id d'un bail d'un locataire
   * @param idLocataire identifiant d'un locataire
   * @returns un identifiant du bail d'un locataire
   */
  getBailIdLocataire(idLocataire: number): Observable<any> {
    return this.http
      .get(this.bailEndpoint + '/bailLocataireId/' + idLocataire)
      .pipe(
        map((jsonResponse: any) => jsonResponse.map((jsonId: any) => jsonId.id))
      );
  }
}
