import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/constants';
import { map } from 'rxjs/operators';
import { Bien } from 'src/app/shared/models/bien.model';
import { environment } from 'src/environments/environment';
import { TypeContrat } from 'src/app/shared/models/typeContrat.model';
import { TypeBien } from 'src/app/shared/models/typeBien.model';
import { Adresse } from 'src/app/shared/models/adresse.model';

/**
 * Définit le content-type du header
 */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
/**
 * Classe Bien service
 */
export class BienService {
  /**
   * Point d'entrée de l'API pour la gestion des bails
   */
  bienEndpoint = environment.API_URL + this.constants.API_ENDPOINTS.BIEN;

  /**
   * Taille maximale du texte du bien affichée dans la mosaïque
   */
  descriptionLimit = 130;

  /**
   * Constructeur du service
   * @param http Service natif d'angular pour la gestion de requêtes HTTP
   * @param constants Constantes de l'application
   */
  constructor(private http: HttpClient, private constants: Constants) {}

  /**
   * Récupère un bien
   * @param id identifiant du bien
   * @returns retourne le bien associé à l'identifiant si le service le trouve
   */
  getBien(id: number): Observable<Bien> {
    return this.http
      .get(this.bienEndpoint + '/' + id)
      .pipe(map((jsonResponse: any) => Bien.fromJson(jsonResponse[0])));
  }

  /**
   * Récupère la liste de tout les biens
   * @returns la liste de tout les biens
   */
  getListeBien(): Observable<any[]> {
    return this.http.get(this.bienEndpoint + '/liste-bien').pipe(
      map((jsonResponse: any) => {
        const listBien: Bien[] = [];
        jsonResponse.forEach((element: any) => {
          const bien: Bien = Bien.fromJson(element);
          if (bien.description.length >= this.descriptionLimit) {
            bien.description =
              bien.description.slice(0, this.descriptionLimit) + '... ';
          }
          listBien.push(bien);
        });
        return listBien;
      })
    );
  }

  /**
   * Récupère la liste des biens suivants des filtres
   * @returns Une liste de bien
   */
  getListeFilterBien(): Observable<any[]> {
    return this.http.get(this.bienEndpoint + '/filter-bien').pipe(
      map((jsonResponse: any) => {
        const listTypeContrat: TypeContrat[] = [];
        const listTypeBien: TypeBien[] = [];
        const listLocalisation: Adresse[] = [];
        jsonResponse[0].listTypeContrat.forEach((element: any) => {
          const typeContrat: TypeContrat = TypeContrat.fromJson(element);
          listTypeContrat.push(typeContrat);
        });
        jsonResponse[0].listTypeBien.forEach((element: any) => {
          const typeBien: TypeBien = TypeBien.fromJson(element);
          listTypeBien.push(typeBien);
        });
        jsonResponse[0].listLocalisation.forEach((element: any) => {
          const localisation: Adresse = Adresse.fromJson(element);
          listLocalisation.push(localisation);
        });
        return [
          {
            listTypeContrat: listTypeContrat,
            listTypeBien: listTypeBien,
            listLocalisation: listLocalisation,
          },
        ];
      })
    );
  }
}
