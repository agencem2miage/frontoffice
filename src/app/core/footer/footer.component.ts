import { Component } from '@angular/core';

import { Constants } from './../../constants';

/**
 * Composant Footer
 */
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  /**
   * Constructeur du composant
   */
  constructor(public constants: Constants) {}
}
