import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

import { TokenService } from '../services/token.service';

/**
 * Guard pour ne pas accéder à la page de connexion administrateur alors qu'on est déjà connecté
 */
@Injectable({
  providedIn: 'root',
})
export class LoggedInUserGuard implements CanActivate {
  /**
   * Constructeur de la guard
   * @param tokenStorageService Service de gestion des tokens
   * @param router Service de navigation natif angular
   */
  constructor(private TokenService: TokenService, private router: Router) { }

  /**
   * Décide si l'utilisateur peut accéder ou non à la page de connexion administrateur
   * @returns booléen
   */
  canActivate(
    route: ActivatedRouteSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // L'admin ne doit pas être connecté pour accéder à cette route
    if (route.data.unloggedUser) {
      if (this.TokenService.getUser() !== null) {
        this.router.navigate(['/user/profil']);
        return false;
      }
      return true;
    }

    if (this.TokenService.getUser() === null) {
      this.router.navigate(['/connexion']);
      return false;
    }
    return true;
  }
}
