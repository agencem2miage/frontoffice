import { Component } from '@angular/core';

import { Constants } from './../../constants';

/**
 * Composant Header
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  /**
   * Constructeur du composant
   */
  constructor(public constants: Constants) {}
}
