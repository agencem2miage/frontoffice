export default class Utils {
  /**
   * Transforme une image en Base 64
   * @param url Url de l'image
   */
  static async imageUrlToBase64(
    url: string
  ): Promise<string | ArrayBuffer | null>;

  /**
   * Transforme une image en Base 64
   * @param url Url de l'iamge
   * @returns Le résultat
   */
  static async imageUrlToBase64(
    url: string
  ): Promise<string | ArrayBuffer | null> {
    const res = await fetch(url);
    const blob = await res.blob();

    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.addEventListener(
        'load',
        function () {
          return resolve(reader.result);
        },
        false
      );

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    });
  }
}
