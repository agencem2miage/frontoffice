import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
})
export class CreateAccountComponent implements OnInit {
  /**
   * Formulaire pour la création de compte
   */
  form!: FormGroup;

  /**
   * Nom du formulaire, ici 'createAccount'
   */
  formLabel = 'createAccount';

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * Constructeur du composant
   * @param fb FormBuilder natif angular
   * @param router Router pour la submission du formulaire
   */
  constructor(private fb: FormBuilder) {}

  /**
   * Initialisation du formulaire
   */
  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.form = this.fb.group({
      creationType: ['', [Validators.required, Validators.maxLength(55)]],
      civilite: ['', [Validators.required, Validators.maxLength(55)]],
      firstName: ['', [Validators.required, Validators.maxLength(55)]],
      lastName: ['', [Validators.required, Validators.maxLength(55)]],
      birthDate: ['', [Validators.required]],
      mail: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      phone: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))s*[)]?[-s.]?[(]?[0-9]{1,3}[)]?([-s.]?[0-9]{3})([-s.]?[0-9]{3,4})'
          ),
        ],
      ],
      numberAdress: ['', [Validators.required, Validators.pattern('[0-9]{5}')]],
      street: ['', [Validators.required, Validators.maxLength(110)]],
      adressComplement: [''],
      city: ['', [Validators.required, Validators.maxLength(55)]],
      postalCode: ['', [Validators.required, Validators.pattern('[0-9]{5}')]],
    });
  }
  
}
