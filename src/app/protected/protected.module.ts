import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { ProtectedRoutingModule } from './protected-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [ProfileComponent],
  imports: [CommonModule, ProtectedRoutingModule, ReactiveFormsModule],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ProtectedModule {}
