import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileComponent } from './profile/profile.component';
import { LoggedInUserGuard } from '../core/guards/logged-in-user.guard';

const routes: Routes = [
  {
    path: 'user',
    redirectTo: 'user/profil',
  },
  {
    path: 'user',
    canActivate: [LoggedInUserGuard],
    children: [
      {
        path: 'profil',
        component: ProfileComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProtectedRoutingModule {}
