import { mergeMap } from 'rxjs/operators';
import { BailService } from './../../core/services/bail.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

import { Constants, Roles } from '../../constants';
import { UserService } from '../../core/services/user.service';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { Modal, ModalType } from 'src/app/shared/models/modal.model';
import { ModalService } from '../../core/services/modal.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import {
  NotificationBackground,
  NotificationIcon,
  Notification,
} from 'src/app/shared/models/notification.model';
import { Role } from 'src/app/shared/models/role.model';
import { Adresse } from 'src/app/shared/models/adresse.model';
import { TokenService } from 'src/app/core/services/token.service';
import { environment } from 'src/environments/environment';
import { FileService } from 'src/app/core/services/file.service';

/**
 * 
 */
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
/**
 * 
 */
export class ProfileComponent implements OnInit {
  constructor(
    private datePipe: DatePipe,
    public constants: Constants,
    private fb: FormBuilder,
    private userService: UserService,
    private fileService: FileService,
    private router: Router,
    private cd: ChangeDetectorRef,
    private modalService: ModalService,
    private notificationsService: NotificationsService,
    private TokenService: TokenService,
    private bailService: BailService
  ) { }

  /**
   * variable temp qui permet de récupérer le user
   */
  user!: Utilisateur;

  /**
   * variable contenant la subscribe de getUser
   */
  getUserSub!: Subscription;

  /**
   * variable contenant la subscribe de getUser
   */
  putUserSub!: Subscription;

  /**
   * variable contenant la subscribe de getListeFileLocation
   */
  getListeFileLocation!: Subscription;

  /**
   * variable contenant la subscribe de getBailSub
   */
  getBailSub!: Subscription;

  /**
   * Formulaire de connexion
   */
  profileForm!: FormGroup;

  /**
   * Formulaire de création de fichiers
   */
  uploadForm!: FormGroup;

  /**
   * Permet d'attendre que les bails soit chargée pour l'afficher
   */
  bailLoaded!: Promise<boolean>;

  /**
   * Clic sur le bouton d'envoi
   */
  submitted = false;

  /**
   * variable contenant la subscribe
   */
  updateFileLocationSub!: Subscription;

  /**
   * identifiant de l'utilisateur
   */
  idUser = this.TokenService.getUser().USER_id;

  /**
   * Tableau d'identifiant des bails
   */
  tabIdBail: number[] = [];

  /**
   * URL
   */
  url_basic: string = environment.FILE_LOCATION_API;

  /**
   * Tableau de la liste 
   */
  tabListFilelocation: any[] = [];

  /**
   * Liste des fichiers de locations
   */
  listFileLocation = {
    listFileBail: [''],
    listFileEtatLieu: [''],
    listFileAssuranceProprietaire: [''],
    listFileAssuranceLocataire: [''],
    listFileContratGestion: [''],
    listFileDiagEnergie: [''],
    listFileAutre: [''],
  };

  /**
   * Liste fichiers bail
   */
  listFileNameBail: Array<string> = [];

  /**
   * Liste fichiers etat des lieux
   */
  listFileNameEtatLieu: Array<string> = [];

  /**
   * Liste assurance propriétaire
   */
  listFileNameAssuranceProprietaire: Array<string> = [];

  /**
   * Liste assurance locataire
   */
  listFileNameAssuranceLocataire: Array<string> = [];

  /**
   * Liste contrat de gestion
   */
  listFileNameContratGestion: Array<string> = [];

  /**
   * Liste diag energie
   */
  listFileNameDiagEnergie: Array<string> = [];

  /**
   * Liste des autres fichiers
   */
  listFileNameAutre: Array<string> = [];

  /**
   * Init du composant
   */
  ngOnInit(): void {
    this.user = Utilisateur.fromJson(this.TokenService.getUser());
    if (this.isProprietaire()) {
      this.getBailSub = this.bailService
        .getBailIdProprietaire(this.idUser)
        .pipe(
          mergeMap((bail: any) => {
            this.tabIdBail = bail;
            return this.fileService.getListeFileLocation(this.tabIdBail);
          })
        )
        .subscribe((file) => {
          this.tabListFilelocation = file;
          this.bailLoaded = Promise.resolve(true);
        });
    } else if (this.isLocataire()) {
      this.getBailSub = this.bailService
        .getBailIdLocataire(this.idUser)
        .pipe(
          mergeMap((bail: any) => {
            this.tabIdBail = bail;
            return this.fileService.getListeFileLocation(this.tabIdBail);
          })
        )
        .subscribe((file) => {
          console.log(file)

          this.tabListFilelocation = file;
          console.log(this.tabListFilelocation)

          this.bailLoaded = Promise.resolve(true);
        });
    }

    this.initForm();
  }

  /**
   * Unsubscribe
   */
  ngOnDestroy(): void {
    this.putUserSub?.unsubscribe();
  }
  /**
   * Initialisation du formulaire
   */
  initForm(): void {
    this.profileForm = this.fb.group({
      role: [this.user.role.label],
      civilite: [this.user.civilite, Validators.required],
      prenom: [
        this.user.prenom,
        [Validators.required, Validators.maxLength(50)],
      ],
      nom: [this.user.nom, [Validators.required, Validators.maxLength(50)]],
      mail: [this.user.mail, [Validators.required, Validators.maxLength(50)]],
      numeroTelephone: [this.user.numeroTelephone, Validators.required],
      dateNaissance: [
        this.datePipe.transform(this.user.dateNaissance, 'yyyy-MM-dd'),
      ],
      villeNaissance: [this.user.villeNaissance, Validators.maxLength(50)],
      numero: [this.user.adresse.numero, Validators.maxLength(50)],
      rue: [this.user.adresse.rue, Validators.maxLength(50)],
      complement: [this.user.adresse.complement, Validators.maxLength(50)],
      codePostal: [
        this.user.adresse.codePostal,
        Validators.pattern('[0-9]{5}'),
      ],
      ville: [this.user.adresse.ville, Validators.maxLength(50)],
      pays: [this.user.adresse.pays, Validators.maxLength(50)],
    });
    this.uploadForm = this.fb.group({
      fileBail: [''],
      fileEtatLieu: [''],
      fileAssuranceProprietaire: [''],
      fileAssuranceLocataire: [''],
      fileContratGestion: [''],
      fileDiagEnergie: [''],
      fileAutre: [''],
    });
  }

  /**
   * Permet de retourner les controls du formulaire facilement
   */
  get f() {
    return this.profileForm.controls;
  }

  /**
   * Permet de retourner les controls du formulaire facilement
   */
  get ff() {
    return this.uploadForm.controls;
  }

  /**
   * méthode appelée lors de la soumission du formulaire.
   * Elle appelle l'API de connexion
   */
  onSubmit(): void {
    this.submitted = true;

    if (this.profileForm.valid) {
      const confirmationModal = new Modal({
        title: 'Confirmation',
        text: 'Êtes-vous sûr de vouloir modifier votre profil ?',
        type: ModalType.CONFIRMATION,
      });

      confirmationModal.confirm = () => {
        const newUser = new Utilisateur({
          id: this.user.id,
          role: Role.fromJson({
            id: this.user.role.id,
            label: this.user.role.label,
          }),
          civilite: this.f.civilite.value,
          nom: this.f.nom.value,
          prenom: this.f.prenom.value,
          adresse: Adresse.fromJson({
            id: this.user.adresse.id,
            numero: this.f.numero.value,
            rue: this.f.rue.value,
            complement: this.f.complement.value,
            codePostal: this.f.codePostal.value,
            ville: this.f.ville.value,
            pays: this.f.pays.value,
          }),
          mail: this.f.mail.value,
          dateNaissance: this.f.dateNaissance.value,
          villeNaissance: this.f.villeNaissance.value,
          numeroTelephone: this.f.numeroTelephone.value,
          motDePasse: '',
        });

        this.putUserSub = this.userService
          .updateUser(newUser)
          .subscribe((jsonResponse) => {
            const notification = new Notification({
              message: 'Votre profil a bien été modifié.',
              background: NotificationBackground.GREEN,
              icon: NotificationIcon.CHECK,
            });
            this.notificationsService.genericNotification(notification);
            this.TokenService.saveUser(jsonResponse);
          });
      };
      this.modalService.confirmationModal(confirmationModal);
    }
  }

  /**
   * Enregistre l'image passée par l'utilisateur en base64
   * @param event Evenement contenant l'image
   */
  onFileChange(event: any, type: string): void {
    if (event.target.files && event.target.files.length) {
      const finalResult: Array<string | ArrayBuffer | null> = [];
      const file = event.target.files;
      const countFile = file.length;
      switch (type) {
        case 'bail': {
          this.listFileNameBail = [];
          break;
        }
        case 'etatLieu': {
          this.listFileNameEtatLieu = [];
          break;
        }
        case 'assuranceProprietaire': {
          this.listFileNameAssuranceProprietaire = [];
          break;
        }
        case 'assuranceLocation': {
          this.listFileNameAssuranceLocataire = [];
          break;
        }
        case 'contratGestion': {
          this.listFileNameContratGestion = [];
          break;
        }
        case 'diagEnergie': {
          this.listFileNameDiagEnergie = [];
          break;
        }
        case 'autre': {
          this.listFileNameAutre = [];
          break;
        }
      }
      for (let i = 0; i < countFile; i++) {
        if (
          file.item(i).type === 'application/pdf' &&
          file.item(i).size <= 5000000
        ) {
          switch (type) {
            case 'bail': {
              this.listFileNameBail.push(file.item(i).name);
              break;
            }
            case 'etatLieu': {
              this.listFileNameEtatLieu.push(file.item(i).name);
              break;
            }
            case 'assuranceProprietaire': {
              this.listFileNameAssuranceProprietaire.push(file.item(i).name);
              break;
            }
            case 'assuranceLocation': {
              this.listFileNameAssuranceLocataire.push(file.item(i).name);
              break;
            }
            case 'contratGestion': {
              this.listFileNameContratGestion.push(file.item(i).name);
              break;
            }
            case 'diagEnergie': {
              this.listFileNameDiagEnergie.push(file.item(i).name);
              break;
            }
            case 'autre': {
              this.listFileNameAutre.push(file.item(i).name);
              break;
            }
          }

          const reader = new FileReader();
          reader.readAsDataURL(file.item(i));
          reader.onload = () => {
            finalResult.push(reader.result);
          };
        } else {
          const notification = new Notification({
            message: "Votre fichier n'est pas valide.",
            background: NotificationBackground.RED,
            icon: NotificationIcon.CROSS,
          });
          this.notificationsService.genericNotification(notification);
        }
      }

      switch (type) {
        case 'bail': {
          this.uploadForm.patchValue({
            fileBail: finalResult,
          });
          break;
        }
        case 'etatLieu': {
          this.uploadForm.patchValue({
            fileEtatLieu: finalResult,
          });
          break;
        }
        case 'assuranceProprietaire': {
          this.uploadForm.patchValue({
            fileAssuranceProprietaire: finalResult,
          });
          break;
        }
        case 'assuranceLocation': {
          this.uploadForm.patchValue({
            fileAssuranceLocataire: finalResult,
          });
          break;
        }
        case 'contratGestion': {
          this.uploadForm.patchValue({
            fileContratGestion: finalResult,
          });
          break;
        }
        case 'diagEnergie': {
          this.uploadForm.patchValue({
            fileDiagEnergie: finalResult,
          });
          break;
        }
        case 'autre': {
          this.uploadForm.patchValue({
            fileAutre: finalResult,
          });
          break;
        }
      }
      this.cd.markForCheck();
    }
  }

  /**
   * Supprime l'image sélectionnée
   */
  onDeleteFile(type: string): void {
    switch (type) {
      case 'bail': {
        this.listFileNameBail = [];
        this.uploadForm.patchValue({
          fileBail: '',
        });
        break;
      }
      case 'etatLieu': {
        this.listFileNameEtatLieu = [];
        this.uploadForm.patchValue({
          fileEtatLieu: '',
        });
        break;
      }
      case 'assuranceProprietaire': {
        this.listFileNameAssuranceProprietaire = [];
        this.uploadForm.patchValue({
          fileAssuranceProprietaire: '',
        });
        break;
      }
      case 'assuranceLocation': {
        this.listFileNameAssuranceLocataire = [];
        this.uploadForm.patchValue({
          fileAssuranceLocataire: '',
        });
        break;
      }
      case 'contratGestion': {
        this.listFileNameContratGestion = [];
        this.uploadForm.patchValue({
          fileContratGestion: '',
        });
        break;
      }
      case 'diagEnergie': {
        this.listFileNameDiagEnergie = [];
        this.uploadForm.patchValue({
          fileDiagEnergie: '',
        });
        break;
      }
      case 'autre': {
        this.listFileNameAutre = [];
        this.uploadForm.patchValue({
          fileAutre: '',
        });
        break;
      }
    }
  }

  /**
   * fonction qui 
   * @param bail Un bail
   * @returns retourne un identifiant d'un bail
   */
  numeroBail(bail: any): number {
    let position = -1;
    for (let i = 0; i < this.tabListFilelocation.length; i++) {
      if (this.tabListFilelocation[i] == bail) {
        position = i;
      }
    }
    return this.tabIdBail[position];
  }

  /**
   * méthode appelée lors de la soumission du formulaire.
   * Elle appelle l'API de connexion
   */
  onSubmitFichiers(bail: any): void {
    this.submitted = true;
    let position = -1;

    //on retrouve l'id du bail. pour ce faire, on retrouve la position de bail en paramètre
    //dans le tableau tabListFilelocation. La position sera la meme dans le tableau de bail.
    for (let i = 0; i < this.tabListFilelocation.length; i++) {
      if (this.tabListFilelocation[i] == bail) {
        position = i;
      }
    }
    const bailId = this.tabIdBail[position];
    if (this.uploadForm.valid) {
      const confirmationModal = new Modal({
        title: 'Confirmation',
        text: 'Êtes-vous sûr de vouloir envoyer ces pièces justificatives ?',
        type: ModalType.CONFIRMATION,
      });

      confirmationModal.confirm = () => {
        const listFile = {
          listFileBail: this.ff.fileBail.value,
          listFileEtatLieu: this.ff.fileEtatLieu.value,
          listFileAssuranceProprietaire: this.ff.fileAssuranceProprietaire
            .value,
          listFileAssuranceLocataire: this.ff.fileAssuranceLocataire.value,
          listFileContratGestion: this.ff.fileContratGestion.value,
          listFileDiagEnergie: this.ff.fileDiagEnergie.value,
          listFileAutre: this.ff.fileAutre.value,
        };
        this.updateFileLocationSub = this.fileService
          .updateFileLocation(listFile, '' + bailId)
          .subscribe(() => {
            const notification = new Notification({
              message: 'Vos fichiers ont bien été ajoutés sur le serveur.',
              background: NotificationBackground.GREEN,
              icon: NotificationIcon.CHECK,
            });
            this.notificationsService.genericNotification(notification);
            this.getListeFileLocation = this.fileService
              .getListeFileLocation(this.tabIdBail)
              .subscribe((result) => {
                this.tabListFilelocation = result;
              });
          });

        this.uploadForm.patchValue({
          fileBail: '',
          fileEtatLieu: '',
          fileAssuranceProprietaire: '',
          fileAssuranceLocataire: '',
          fileContratGestion: '',
          fileDiagEnergie: '',
          fileAutre: '',
        });
      };
      this.modalService.confirmationModal(confirmationModal);
    }
  }

  /**
   * Permet d'afficher ou de ne pas afficher les informations des bails si il existe des bails
   */
  isBail(): boolean {
    if (this.tabIdBail.length == 0) {
      return false;
    }
    return true;
  }

  /**
   * permet de savoir si l'utilisateur est propriétaire, afin de lui afficher uniquement ses documents propres
   * @return si l'utilisateur est propriétaire ou non
   */
  isProprietaire(): boolean {
    if (Roles[this.TokenService.getUser().ROLE_id] == 'PROPRIETAIRE') {
      return true;
    }
    return false;
  }

  /**
   * Permet de savoir si un utlisateur est locataire, afin de lui afficher uniquement ses documents propres
   * @returns si l'utilisateur est locataire ou non
   */
  isLocataire(): boolean {
    if (Roles[this.TokenService.getUser().ROLE_id] == 'LOCATAIRE') {
      return true;
    }
    return false;
  }
}
