import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgMeta } from 'ngmeta';
import { registerLocaleData, DatePipe } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ProtectedModule } from './protected/protected.module';
import { PublicModule } from './public/public.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ProtectedModule,
    PublicModule,
    AppRoutingModule,
    CoreModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'fr-FR' }, NgMeta, DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
