import { Bien } from './bien.model';
import { Utilisateur } from './utilisateur.model';
export class Bail {
  /**
   * Date de prise d'effet du bail
   */
  datePriseEffet: string;

  /**
   * Durée du bail
   */
  dureeBail: string;

  /**
   * Durée du bail si dureeBail réduite
   */
  dureeBailReduite: string;

  /**
   * Justification de la réduction de la durée du bail
   */
  justificationDureeBailReduite: string;

  /**
   * Loyer mensuel HT
   */
  loyerHorsCharges: number;

  /**
   * Montant des charges récupérables
   */
  chargesRecuperables: number;

  /**
   * Le loyer du logement objet du présent contrat est soumis au décret fixant annuellement le montant maximum d'évolution des loyers à la relocation
   */
  isMontantMaximumAnnuel: boolean;

  /**
   * Le loyer du logement objet du présent contrat est soumis au loyer de référence majoré fixé par arrêté préfectoral
   */
  isLoyerReference: boolean;

  /**
   * Montant du loyer de référence
   */
  loyerReference: number;

  /**
   * Périodicité de révision du loyer
   */
  periodicite: string;

  /**
   * Montant du dernier loyer de l'ancien locataire
   */
  loyerAncienLocataire: number;

  /**
   * Modalité de règlement des charges récupérables
   */
  modaliteReglementChargesRecuperables: string;

  /**
   * Montant des charges récupérables
   */
  montantChargesRecuperables: number;

  /**
   * En cas de colocation, le bailleur a-t-il souscrit une assurance pour les locataires ?
   */
  souscriptionAssuranceParBailleur: boolean;

  /**
   * Périodicité du paiement du loyer
   */
  periodicitePaiementLoyer: string;

  /**
   * Echeance du paiement
   */
  echeancePaiement: string;

  /**
   * Date ou période de paiement
   */
  datePeriodePaiement: string;

  /**
   * Montant de la première échéance
   */
  montantPremiereEcheance: number;

  /**
   * Montant total annuel récupérable au titre de l’assurance pour compte des colocataires
   */
  montantRecuperableAssurance: number;

  /**
   * Montant récupérable par douzième
   */
  montantRecuperableAssuranceDouzieme: number;

  /**
   * Liste des majorations liées à des travaux
   */
  majorations: Majoration[];

  /**
   * Montant des garanties
   */
  montantGaranties: number;

  /**
   * "habitation" ou "mixte professionnel et habitation"
   * usage du bien pour
   */
  usageLocaux: string;

  /**
   * Signature du propriétaire (oui ou non)
   */
  signatureProprietaire: boolean;

  /**
   * Signature du locataire (oui ou non)
   */
  signatureLocataire: boolean;

  /**
   * Nom du pdf
   */
  pdf: string;

  /**
   * Bien du bail
   */
  bien: Bien;
  /**
   * Locataire du bail
   */
  locataire: Utilisateur;
  /**
   * Constructeur de l'objet Bail
   * @param options Propriétés de l'objet
   */
  constructor(options: {
    datePriseEffet: string;
    dureeBail: string;
    dureeBailReduite: string;
    justificationDureeBailReduite: string;
    loyerHorsCharges: number;
    chargesRecuperables: number;
    isMontantMaximumAnnuel: boolean;
    isLoyerReference: boolean;
    loyerReference: number;
    periodicite: string;
    loyerAncienLocataire: number;
    modaliteReglementChargesRecuperables: string;
    montantChargesRecuperables: number;
    souscriptionAssuranceParBailleur: boolean;
    periodicitePaiementLoyer: string;
    echeancePaiement: string;
    datePeriodePaiement: string;
    montantPremiereEcheance: number;
    montantRecuperableAssurance: number;
    montantRecuperableAssuranceDouzieme: number;
    majorations: Majoration[];
    montantGaranties: number;
    usageLocaux: string;
    signatureProprietaire: boolean;
    signatureLocataire: boolean;
    pdf: string;
    locataire: Utilisateur;
    bien: Bien;
  }) {
    this.datePriseEffet = options.datePriseEffet || '';
    this.dureeBail = options.dureeBail || '';
    this.dureeBailReduite = options.dureeBailReduite || '';

    this.justificationDureeBailReduite =
      options.justificationDureeBailReduite || '';

    this.loyerHorsCharges = options.loyerHorsCharges || 0;
    this.chargesRecuperables = options.chargesRecuperables || 0;
    this.isMontantMaximumAnnuel = options.isMontantMaximumAnnuel || false;
    this.isLoyerReference = options.isLoyerReference || false;
    this.loyerReference = options.loyerReference || 0;
    this.periodicite = options.periodicite || '';
    this.loyerAncienLocataire = options.loyerAncienLocataire || 0;
    this.modaliteReglementChargesRecuperables =
      options.modaliteReglementChargesRecuperables || '';
    this.montantChargesRecuperables = options.montantChargesRecuperables || 0;
    this.souscriptionAssuranceParBailleur =
      options.souscriptionAssuranceParBailleur || false;
    this.periodicitePaiementLoyer = options.periodicitePaiementLoyer || '';
    this.echeancePaiement = options.echeancePaiement || '';
    this.datePeriodePaiement = options.datePeriodePaiement || '';
    this.montantPremiereEcheance = options.montantPremiereEcheance || 0;
    this.montantRecuperableAssurance = options.montantRecuperableAssurance || 0;

    this.montantRecuperableAssuranceDouzieme =
      options.montantRecuperableAssuranceDouzieme || 0;

    this.majorations = options.majorations || [];

    this.montantGaranties = options.montantGaranties || 0;
    this.usageLocaux = options.usageLocaux || '';
    this.signatureProprietaire = options.signatureProprietaire || false;
    this.signatureLocataire = options.signatureLocataire || false;
    this.pdf = options.pdf || '';
    this.locataire = options.locataire || '';
    this.bien = options.bien || '';
  }

  /**
   * Transforme l'objet bail en json
   * @param json le json à transformer
   * @returns l'objet en json
   */
  public static fromJson(json: any): Bail {
    return new Bail({
      datePriseEffet: json.datePriseEffet,
      dureeBail: json.dureeBail,
      dureeBailReduite: json.dureeBailReduite,
      justificationDureeBailReduite: json.justificationDureeBailReduite,
      loyerHorsCharges: json.loyerHorsCharges,
      chargesRecuperables: json.chargesRecuperables,
      isMontantMaximumAnnuel: json.isMontantMaximumAnnuel,
      isLoyerReference: json.isLoyerReference,
      loyerReference: json.loyerReference,
      periodicite: json.periodicite,
      loyerAncienLocataire: json.loyerAncienLocataire,
      modaliteReglementChargesRecuperables:
        json.modaliteReglementChargesRecuperables,
      montantChargesRecuperables: json.montantChargesRecuperables,
      souscriptionAssuranceParBailleur: json.souscriptionAssuranceParBailleur,
      periodicitePaiementLoyer: json.periodicitePaiementLoyer,
      echeancePaiement: json.echeancePaiement,
      datePeriodePaiement: json.datePeriodePaiement,
      montantPremiereEcheance: json.montantPremiereEcheance,
      montantRecuperableAssurance: json.montantRecuperableAssurance,
      montantRecuperableAssuranceDouzieme:
        json.montantRecuperableAssuranceDouzieme,
      majorations: json.majorations,
      montantGaranties: json.montantGaranties,
      usageLocaux: json.usageLocaux,
      signatureProprietaire: json.signatureProprietaire,
      signatureLocataire: json.signatureLocataire,
      pdf: json.pdf,
      locataire: json.locataire,
      bien: json.bien,
    });
  }
}

/**
 * Interface de majoration
 */
export interface Majoration {
  /**
   * Nature de majoration
   */
  nature: string;

  /**
   * Modalité de la majoration
   */
  modalites: string;

  /**
   * Delai de réalisation de majoration
   */
  delaiRealisation: string;

  /**
   * montant de la majoration
   */
  montant: string;
}
