import { environment } from 'src/environments/environment';
import { Adresse } from './adresse.model';
import { legalRegime } from './legalRegime.model';
import { TypeBien } from './typeBien.model';
import { TypeContrat } from './typeContrat.model';
import { Utilisateur } from './utilisateur.model';

/**
 * Modèle représentant un Bien
 */
export class Bien {
  /**
   * Identifiant du Bien
   */
  readonly id: number;

  /**
   * Utilisateur propriétaire du bien
   */
  proprietaire: number;

  /**
   * Type de contrat : vente ou location
   */
  typeContrat: TypeContrat;

  /**
   * Type de bien : Maison, Appartement, Terrain, Parking, Autre
   */
  typeBien: TypeBien;

  /**
   * Adresse du bien
   */
  adresse: Adresse;

  /**
   * Régime juridique du bien : mono ou copropriété
   */
  regimeJuridique: legalRegime;

  /**
   * Pièces du bien
   */
  pieces: Piece[];

  /**
   * Annee de construction du bien
   */
  anneeConstruction: number;

  /**
   * Surface habitable du bien
   */
  surfaceHabitable: number;

  /**
   * Surface du terrain
   */
  surfaceTerrain: number;

  /**
   * Surface du balcon
   */
  surfaceBalcon: number;

  /**
   * Nombre de pièces
   */
  nombrePiece: number;

  /**
   * Nombre de chambres
   */
  nombreChambre: number;

  /**
   * prix du bien, correspond soit au prix de vente soit au prix du loyer (avec charges), selon le type de contrat
   */
  prix: number;

  /**
   * Equipement de la maison (cuisine etc...)
   */
  equipementMaison: string;

  /**
   * Détail supplémentaire sur les pièces au format texte pour l'agent immobilier
   * Correspond à la mention "autre parties du logement" du bail
   */
  detailPieces: string;

  /**
   * Nombre de salle de bains du bien
   */
  nombreSalleDeBain: number;

  /**
   * bien meublé ou non meublé
   */
  meuble: boolean;

  /**
   * description du bien supplémentaire, champs libre pour l'agent immobilier
   */
  description: string;

  /**
   * classe énergie du bien, lettre de A à G ou "vierge"
   */
  classeEnergie: string;

  /**
   * GES du bien, lettre de A à G ou "vierge"
   */
  ges: string;

  /**
   * adresse de stockage des images du bien, 0 à 10 images max
   */
  image: string[];

  /**
   * présense ou non d'un garage à vélo
   */
  garageVelo: boolean;

  /**
   * présence ou non d'un garage
   */
  garageVoiture: boolean;

  /**
   * Présence d'espace vert
   */
  espacesVerts: boolean;

  /**
   * Présence de locaux poubelles
   */
  localPoubelles: boolean;

  /**
   * Présence d'un ascenceur
   */
  ascenseur: boolean;

  /**
   * Présence d'un air de jeu
   */
  aireJeux: boolean;

  /**
   * présence d'un gardiennage
   */
  gardiennage: boolean;

  /**
   * présence d'une laverie
   */
  laverie: boolean;

  /**
   * Liste des travaux effectués ainsi que le montant lié
   */
  travaux: Travaux[];

  /**
   * Constructeur de l'objet, tous les paramètres sont obligatoires
   * @param options
   */
  constructor(option: {
    id: number;
    anneeConstruction: number;
    surfaceHabitable: number;
    surfaceTerrain: number;
    surfaceBalcon: number;
    nombrePiece: number;
    nombreChambre: number;
    prix: number;
    equipementMaison: string;
    detailPieces: string;
    nombreSalleDeBain: number;
    meuble: boolean;
    description: string;
    classeEnergie: string;
    ges: string;
    image: string[];
    garageVelo: boolean;
    garageVoiture: boolean;
    espacesVerts: boolean;
    localPoubelles: boolean;
    ascenseur: boolean;
    aireJeux: boolean;
    gardiennage: boolean;
    laverie: boolean;
    regimeJuridique: legalRegime;
    proprietaire: number;
    typeContrat: TypeContrat;
    typeBien: TypeBien;
    adresse: Adresse;
    pieces?: Piece[];
    travaux: Travaux[];
  }) {
    this.id = option.id || 0;
    this.anneeConstruction = option.anneeConstruction || 0;
    this.surfaceHabitable = option.surfaceHabitable || 0;
    this.surfaceTerrain = option.surfaceTerrain || 0;
    this.surfaceBalcon = option.surfaceBalcon || 0;
    this.nombrePiece = option.nombrePiece || 0;
    this.nombreChambre = option.nombreChambre || 0;
    this.prix = option.prix || 0;
    this.equipementMaison = option.equipementMaison || '';
    this.detailPieces = option.detailPieces || '';
    this.nombreSalleDeBain = option.nombreSalleDeBain || 0;
    this.meuble = option.meuble || false;
    this.description = option.description || '';
    this.classeEnergie = option.classeEnergie || '';
    this.ges = option.ges || '';
    this.image = option.image || '';
    this.garageVelo = option.garageVelo || false;
    this.garageVoiture = option.garageVoiture || false;
    this.espacesVerts = option.espacesVerts || false;
    this.localPoubelles = option.localPoubelles || false;
    this.ascenseur = option.ascenseur || false;
    this.aireJeux = option.aireJeux || false;
    this.gardiennage = option.gardiennage || false;
    this.laverie = option.laverie || false;
    this.typeContrat = option.typeContrat || '';
    this.typeBien = option.typeBien || '';
    this.regimeJuridique = option.regimeJuridique || '';
    this.proprietaire = option.proprietaire || -1;
    this.adresse = option.adresse || '';
    this.pieces = option.pieces || [];
    this.travaux = option.travaux || [];
  }

  /**
   * Crée un administrateur à partir d'un flux  JSON
   * @param json Les propriétés de l'administrateur contenues dans le flux JSON
   * @returns {Administrator} L'objet administrateur créé
   */
  public static fromJson(json: any): Bien {
    let temp;
    if (json.PREMIERE_IMAGE) {
      temp = [environment.IMAGE_API + json.PREMIERE_IMAGE];
    }
    return new Bien({
      id: json.BIEN_ID,
      proprietaire: json.proprietaire, //n'est pas récupéré
      typeContrat: TypeContrat.fromJson({
        id: json.ID_TYPE_CONTRAT,
        label: json.TYPE_CONTRAT_LABEL,
      }),
      typeBien: TypeBien.fromJson({
        id: json.ID_TYPE_BIEN,
        label: json.TYPE_BIEN_LABEL,
      }),
      adresse: Adresse.fromJson({
        id: json.BIEN_ID_ADRESSE,
        codePostal: json.ADRESSE_BIEN_CODE_POSTAL,
        ville: json.ADRESSE_BIEN_VILLE,
      }),
      regimeJuridique: legalRegime.fromJson({
        label: json.REGIME_JURIDIQUE_LABEL,
      }),
      anneeConstruction: json.ANNEE_CONSTRUCTION,
      surfaceHabitable: json.SURFACE_HABITABLE,
      surfaceTerrain: json.SURFACE_TERRAIN,
      surfaceBalcon: json.SURFACE_BALCON,
      nombrePiece: json.NB_PIECE,
      nombreChambre: json.NB_CHAMBRE,
      prix: json.BIEN_PRIX_LOCATION,
      equipementMaison: json.EQUIPEMENT,
      detailPieces: json.DETAILS_PIECE,
      nombreSalleDeBain: json.NB_SALLE_DE_BAIN,
      meuble: json.MEUBLE,
      description: json.DESCRIPTION,
      classeEnergie: json.CLASSE_ENERGIE,
      ges: json.GES,
      image: json.IMAGES || temp,
      garageVelo: json.GARAGE_VELO,
      garageVoiture: json.GARAGE_VOITURE,
      espacesVerts: json.ESPACES_VERTS,
      localPoubelles: json.LOCAL_POUBELLE,
      ascenseur: json.ASCENSEUR,
      aireJeux: json.AIRE_JEUX,
      gardiennage: json.GARDIENNAGE,
      laverie: json.LAVERIE,
      travaux: json.TRAVAUX,
    });
  }
}

/**
 * Interface Piece
 */
export interface Piece {
  /**
   *  Identifiant nombre
   */

  id: number;
  /**
   * Label de la pièce
   */
  label: string;
}

/**
 * Interface travaux
 */
export interface Travaux {
  /**
   * Montant des travaux
   */
  montant: number;

  /**
   * La nature des travaux
   */
  nature: string;
}
