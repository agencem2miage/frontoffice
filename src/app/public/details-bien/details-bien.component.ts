import { ImagesService } from './../../core/services/images.service';
import { Component, OnInit } from '@angular/core';
import { NgMeta } from 'ngmeta';
import { Bien } from 'src/app/shared/models/bien.model';
import { Constants } from '../../constants';
import { BienService } from 'src/app/core/services/bien.service';
import { ActivatedRoute } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-details-bien',
  templateUrl: './details-bien.component.html',
  styleUrls: ['./details-bien.component.scss'],
})
export class DetailsBienComponent implements OnInit {
  /**
   * Tableau d'images qui seront affichées dans le slider
   */
  imageObject: Array<any> = [];

  /**
   * Image d'accueil
   */
  imageAccueil = '';

  /**
   * Souscription au service de récupération du bien
   */
  getBienSub: Subscription = new Subscription();

  /**
   * Souscription au service de récupération des images
   */
  getImagesSub: Subscription = new Subscription();

  /**
   * Permet d'attendre que le bien soit chargée pour l'afficher
   */
  bienLoaded!: Promise<boolean>;

  /**
   * Permet d'attendre que les images soit chargée pour l'afficher
   */
  imagesLoaded!: Promise<boolean>;

  /**
   * Le Bien dont le détail est affichée
   */
  bien!: Bien;

  /**
   * Constructeur du composant
   * @param constants Constants
   * @param route Route
   * @param ngMeta NgMeta
   * @param bienService Service pour les biens
   * @param imageService Service pour les images
   */
  constructor(
    public constants: Constants,
    private route: ActivatedRoute,
    private ngMeta: NgMeta,
    private bienService: BienService,
    private imageService: ImagesService
  ) { }

  /**
   * Initialisation du composant
   */
  ngOnInit(): void {
    this.ngMeta.setAll({
      title: this.constants.SEO_SITE_TITLE + 'Détails du bien',
      description: this.constants.SEO_SITE_DESCRIPTION,
      canonical: this.constants.CURRENT_URL,
      image: this.constants.IMAGE_URL + 'logo.png',
    });
    this.getBienSub = this.bienService
      .getBien(Number(this.route.snapshot.paramMap.get('id')))
      .pipe(
        mergeMap((bien: Bien) => {
          if (bien) {
            this.bien = bien;
            this.bienLoaded = Promise.resolve(true);
            return this.imageService.getImages(
              Number(this.route.snapshot.paramMap.get('id'))
            );
          }
          return of(null);
        })
      )
      .subscribe((result: string[]) => {
        this.bien.image = result;
        this.chargerImageSlider();
        this.imagesLoaded = Promise.resolve(true);
      });
  }

  /**
   * méthode permettant de savoir si le bien est un appartement ou non, pour afficher des caractéristiques propres aux appartements
   * @returns boolean
   */
  isAppartement(): boolean {
    if (this.bien.typeBien.label == 'Appartement') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Méthode permettant de savoir si le bien a un terrain ou non, pour afficher la présence d'un terrain
   * @returns boolean
   */
  isTerrain(): boolean {
    if (this.bien.surfaceTerrain != 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Méthode permettant de savoir si le bien à un balcon, pour afficher sa présence ou non
   * @returns boolean
   */
  isBalcon(): boolean {
    if (this.bien.surfaceBalcon != 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * méthode permettant de récupérer les images du bien, et de les mettre dans le tableau d'image pour le slider
   */
  chargerImageSlider(): void {
    const imageSlider = {
      image: '',
      thumbImage: '',
      order: 0,
    };
    let compteur = 0;

    //récupération des images de l'objet bien
    this.bien.image.forEach((image_bdd: any) => {
      // si première image → image principal en gros
      if (compteur == 0) {
        this.imageAccueil = environment.IMAGE_API + image_bdd.label;
        compteur += 1;
        // si pas première image → on le met dans le slider
      } else {
        const newImageSlider = Object.create(imageSlider);
        newImageSlider.image = environment.IMAGE_API + image_bdd.label;
        newImageSlider.thumbImage = environment.IMAGE_API + image_bdd.label;
        newImageSlider.order = compteur;
        //on met l'objet dans le tableau d'image du slider
        this.imageObject.push(newImageSlider);
        compteur += 1;
      }
    });
    if (compteur == 0) {
      this.imageAccueil = this.constants.IMAGE_URL + 'erreur_image.PNG';
    }
  }
}
