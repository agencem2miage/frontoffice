import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { CguComponent } from './cgu/cgu.component';
import { HomeComponent } from './home/home.component';
import { LegalNoticeComponent } from './legal-notice/legal-notice.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PublicRoutingModule } from './public-routing.module';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { DetailsBienComponent } from './details-bien/details-bien.component';
import { ListBienComponent } from './list-bien/list-bien.component';
import { NgImageSliderModule } from 'ng-image-slider';

@NgModule({
  declarations: [
    HomeComponent,
    NotFoundComponent,
    CguComponent,
    LegalNoticeComponent,
    PersonalDataComponent,
    ConnexionComponent,
    DetailsBienComponent,
    ListBienComponent,
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    SharedModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    NgImageSliderModule,
    BrowserAnimationsModule,
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class PublicModule {}
