import { Component, OnInit } from '@angular/core';
import { NgMeta } from 'ngmeta';

import { Constants } from '../../constants';

/**
 * Url du component
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

/**
 * Classe Home component
 */
export class HomeComponent implements OnInit {

  /**
   * Constructeur du composant
   * @param constants Constants
   * @param ngMeta NgMeta
   */
  constructor(public constants: Constants, private ngMeta: NgMeta) {}

  /**
   * Initialisation du composant
   */
  ngOnInit(): void {
    this.ngMeta.setAll({
      title: this.constants.SEO_SITE_TITLE + 'Accueil',
      description: this.constants.SEO_SITE_DESCRIPTION,
      canonical: this.constants.CURRENT_URL,
      image: this.constants.IMAGE_URL + 'logo.png',
    });
  }
}
