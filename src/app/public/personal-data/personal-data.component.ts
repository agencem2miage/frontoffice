import { Component, OnInit } from '@angular/core';
import { NgMeta } from 'ngmeta';

import { Constants } from '../../constants';

/**
 * Lien du component
 */
@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
})

/**
 * Class Personal Data
 */
export class PersonalDataComponent implements OnInit {
  
  /**
   * Constructeur du component Personal Data
   * @param constants Constantes
   * @param ngMeta Meta
   */
  constructor(public constants: Constants, private ngMeta: NgMeta) {}

  /**
   * Initialisation du composant
   */
  ngOnInit(): void {
    this.ngMeta.setAll({
      title: this.constants.SEO_SITE_TITLE + 'Accueil',
      description: this.constants.SEO_SITE_DESCRIPTION,
      canonical: this.constants.CURRENT_URL,
      image: this.constants.IMAGE_URL + 'logo.png',
    });
  }
}
