import { ConnexionComponent } from './connexion/connexion.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CguComponent } from './cgu/cgu.component';
import { HomeComponent } from './home/home.component';
import { LegalNoticeComponent } from './legal-notice/legal-notice.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { DetailsBienComponent } from './details-bien/details-bien.component';
import { ListBienComponent } from './list-bien/list-bien.component';
import { LoggedInUserGuard } from '../core/guards/logged-in-user.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'accueil',
    pathMatch: 'full',
  },
  {
    path: 'accueil',
    component: HomeComponent,
  },
  {
    path: 'page-404',
    component: NotFoundComponent,
  },
  {
    path: 'mentions-legales',
    component: LegalNoticeComponent,
  },
  {
    path: 'conditions-generales-d-utilisation',
    component: CguComponent,
  },
  {
    path: 'donnees-personnelles',
    component: PersonalDataComponent,
  },
  {
    path: 'connexion',
    component: ConnexionComponent,
    canActivate: [LoggedInUserGuard],
    data: { unloggedUser: true },
  },
  {
    path: 'details-bien/:id',
    component: DetailsBienComponent,
  },
  {
    path: 'liste-biens',
    component: ListBienComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule {}
