import { browser, by, element } from 'protractor';

/**
 * Class AppPage
 */
export class AppPage {

  /**
   * Fonction qui permet de naviguer 
   * @returns retourne l'url de navigation
   */
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  /**
   * Recupère le titre d'un texte
   * @returns retourne le texte
   */
  async getTitleText(): Promise<string> {
    return element(by.css('app-root .content span')).getText();
  }
}
